# dreel-install

## Не работал на убунту 20.04, решение

```
sudo apt-get install -y libsdl-pango-dev libcanberra-gtk-module make gcc
cd ~
curl -JOL http://ftp.acc.umu.se/pub/GNOME/sources/pango/1.42/pango-1.42.4.tar.xz
tar xf pango-1.42.4.tar.xz
cd pango-1.42.4
printf "all:\n\ttrue\n\ninstall:\n\ttrue\n\n" > tests/Makefile.in
./configure --prefix=/usr
sudo mkdir /usr/lib/oldpango
sudo chmod 777 -R /usr/lib/oldpango/
make && make DESTDIR="/usr/lib/oldpango" install
```


+ создать баш файл
```
cd ~
sudo nano dreel.sh
```

```
#!/bin/bash
LD_LIBRARY_PATH=/usr/lib/oldpango/usr/lib /usr/local/bin/dreel
```


- https://bbs.archlinux.org/viewtopic.php?id=249604
- https://stackoverflow.com/questions/13428910/how-to-set-the-environmental-variable-ld-library-path-in-linux

## Установка зависимостей
```
TMPDIR=$(mktemp -d dreel_install.XXX)
cd $TMPDIR

wget http://ftp.ru.debian.org/debian/pool/main/g/gconf/gconf2_3.2.6-8_amd64.deb
wget http://ftp.ru.debian.org/debian/pool/main/g/gconf/libgconf-2-4_3.2.6-8_amd64.deb
wget http://ftp.ru.debian.org/debian/pool/main/g/gconf/gconf2-common_3.2.6-8_all.deb
wget http://ftp.ru.debian.org/debian/pool/main/g/gconf/gconf-service_3.2.6-8_amd64.deb
wget http://ftp.ru.debian.org/debian/pool/main/o/openldap/libldap-2.5-0_2.5.13%2bdfsg-5_amd64.deb

wget http://ftp.ru.debian.org/debian/pool/main/liba/libayatana-appindicator/libayatana-appindicator1_0.5.92-1_amd64.deb
wget http://ftp.ru.debian.org/debian/pool/main/libd/libdbusmenu/libdbusmenu-gtk4_18.10.20180917~bzr492+repack1-3_amd64.deb
wget http://ftp.ru.debian.org/debian/pool/main/liba/libayatana-indicator/libayatana-indicator7_0.6.2-3_amd64.deb


sudo dpkg -i gconf2-common_3.2.6-8_all.deb || exit 1
sudo dpkg -i libgconf-2-4_3.2.6-8_amd64.deb || exit 1
sudo dpkg -i libldap-2.5-0_2.5.13+dfsg-5_amd64.deb || exit 1
sudo dpkg -i gconf-service_3.2.6-8_amd64.deb || exit 1
sudo dpkg -i gconf2_3.2.6-8_amd64.deb || exit 1

sudo dpkg -i libayatana-indicator7_0.6.2-3_amd64.deb || exit 1
sudo dpkg -i libdbusmenu-gtk4_18.10.20180917~bzr492+repack1-3_amd64.deb || exit 1
sudo dpkg -i libayatana-appindicator1_0.5.92-1_amd64.deb || exit 1

rm gconf2-common_3.2.6-8_all.deb \
    libgconf-2-4_3.2.6-8_amd64.deb \
    libldap-2.5-0_2.5.13+dfsg-5_amd64.deb \
    gconf-service_3.2.6-8_amd64.deb \
    gconf2_3.2.6-8_amd64.deb \
    libayatana-indicator7_0.6.2-3_amd64.deb \
    libdbusmenu-gtk4_18.10.20180917~bzr492+repack1-3_amd64.deb \
    libayatana-appindicator1_0.5.92-1_amd64.deb

rmdir $TMPDIR
```
